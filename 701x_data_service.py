import os
import shutil
import glob
import time
import requests

sleep_time = 30
search_dir = "/home/azureuser/FTP/*/*.csv"

sync_dir_name = "synced_logs"
sync_dir = ""
sync_path = ""

file_list = ""
file_url = "https://api.701x.com/TagDataFile/upload"


while True:

	file_list = glob.glob(search_dir, recursive=True)

	if file_list:
		print("Found " + str(len(file_list)) + " new files to upload.")

		for file in file_list:
			print("File to upload: " + str(file))

			log_file = {"file": open(file, "rb")}
			req = requests.post(file_url, files=log_file)

			if req.status_code == 200:
				print("Upload successful, removing log.")

				sync_dir = os.path.join(os.path.dirname(file), sync_dir_name)
				if not os.path.isdir(sync_dir):
					os.mkdir(sync_dir)
					print("Created synced logs directory.")

				sync_path = os.path.join(os.path.dirname(file), sync_dir_name, os.path.basename(file))
				print(sync_path)

				shutil.move(file, sync_path)

			else:
				print("POST Request Status: " + str(req.status_code))
				print("POST Request Text: " + req.text)

	# else:
		# print("No new files to upload.")


	# print("Sleeping for " + str(sleep_time) + " seconds.")
	time.sleep(sleep_time)


exit(0)
